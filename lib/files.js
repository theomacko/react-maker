const fs = require('fs')
const touch = require('touch')

function completeText(componentName) {
  return `const ${componentName} = () => {\n\treturn (\n\t\t<div>\n\n\t\t</div>\n\t)\n}\n\nexport default ${componentName}`
}

module.exports = {
  fileExists: (targetPath) => {
    return fs.existsSync(targetPath)
  },
  makeComponent: (details) => {
    let currentPath = 'src/components'

    fs.mkdir(
      `${currentPath}/${details.componentName}`,
      { recursive: true },
      (err) => {
        if (err) throw err

        touch.sync(`${currentPath}/${details.componentName}/index.jsx`)

        fs.open(
          `${currentPath}/${details.componentName}/index.jsx`,
          'r+',
          666,
          (err, id) => {
            fs.write(
              id,
              completeText(details.componentName),
              (err, written, str) => {
                fs.close(id, (err) => {
                  if (err) throw err
                })
              }
            )
          }
        )
      }
    )
  },
}
