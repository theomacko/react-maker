const inquirer = require('inquirer')

module.exports = {
  askForDetails: () => {
    return inquirer.prompt([
      {
        name: 'componentName',
        type: 'input',
        message: 'Enter the name of the Component you wish to create: ',
        validate: function (value) {
          if (typeof value === 'string') {
            return true
          } else {
            return 'Please enter a valid Component name'
          }
        },
      },
    ])
  },
}
