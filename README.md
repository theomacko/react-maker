# react-maker

react-maker is a Node JS tool, usable with bash, that gives you access to a cli in order to create new react component files.

## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install react-maker.

```yarn
cd into/project
yarn install
yarn link
```

## Usage

```bash
react-maker
```

Follow further instructions

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)