#!/usr/bin/env node

const chalk = require('chalk')
const clear = require('clear')
const figlet = require('figlet')

const files = require('./lib/files')
const inquirer = require('./lib/inquirer')

clear()

console.log(
  chalk.blueBright(
    figlet.textSync('REACT MAKER', {
      horizontalLayout: 'full',
    })
  )
)

if (!files.fileExists('src')) {
  console.log(chalk.inverse.red('No "src" folder found, set terminal to project root'))
} else {
  const run = async () => {
    let details = await inquirer.askForDetails()
    files.makeComponent(details)

    console.log(chalk.inverse.green(`The component ${details.componentName} was created successfully !`))
    console.log('\n')
  }

  run()
}
